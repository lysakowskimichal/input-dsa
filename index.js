document.addEventListener('DOMContentLoaded', function () {
    const container = document.getElementById("container")
    const input = document.getElementById("siema")
    const placeholder = document.createElement("span")
    placeholder.innerText = `*`
    placeholder.classList.add("placeholder")
    placeholder.style.color = "red"
    placeholder.style.display = "inline-block"

    
    container.append(placeholder)

    document.addEventListener("change", function(){
        if(input.value.length > 0){
            placeholder.style.display = "none"
        } else {
            placeholder.style.display = "block"
        }
    })
} )

